# Compilation options
F90 := mpiifort
F90FLAGS := -O2 -g -fPIC -mkl=cluster -xCORE-AVX512 -align array64byte
FPPFLAGS := -DMW_CI -DMW_USE_PLUMED
LDFLAGS := 
F2PY := f2py
F90WRAP := f90wrap
FCOMPILER := intelem
J := -module 

# Path to pFUnit (Unit testing Framework)
PFUNIT := $(PFUNIT)
