#!/bin/bash
#PBS -q alpha
#PBS -l select=1:ncpus=32
#PBS -l walltime=0:10:00
#PBS -N your_job_name
#PBS -j oe

# load modules
module purge
module load intel/intel-compilers-18.2/18.2
module load intel/intel-cmkl-18.2/18.2
module load intel/intel-mpi/2018.2

#move to PBS_O_WORKDIR
cd "$PBS_O_WORKDIR"
echo `pwd`

# Define scratch space scratchalpha
SCRATCH="/scratchalpha/$USER/job-$PBS_JOBID"
rm -rf "$SCRATCH"
mkdir "$SCRATCH"
cd "$SCRATCH"
echo `pwd`

# Copy some input files to $SCRATCH directory
cp "$PBS_O_WORKDIR"/your_input .
 
# Execute your program
mpirun -n 32 /path/to/mw/executable

# Move to PBS_O_WORKDIR and copy output
cd "$PBS_O_WORKDIR"
cp "$SCRATCH"/your_output  .
rm -rf "$SCRATCH"

exit 0
