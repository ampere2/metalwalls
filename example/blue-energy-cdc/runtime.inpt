# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps       100 # number of steps in the run
timestep     41.341 # timestep (a.u)
temperature   298.0 # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  2

# Thermostat:
# -----------
thermostat
  chain_length  5
  relaxation_time 4134.1
  tolerance  1.0e-17
  max_iteration  100

# Species definition
# ----------------------
species

  species_type
    name   O               # name of the species
    count  8800            # number of species
    mass   15.996          # mass in amu
    mobile True  
    charge point -0.8476   # permanent charge on the ions

  species_type
    name   H1              # name of the species
    count  8800            # number of species
    mass   1.0008          # mass in amu
    mobile True
    charge point 0.4238    # permanent charge on the ions

  species_type
    name   H2              # name of the species
    count  8800            # number of species 
    mass   1.0008          # mass in amu
    mobile True
    charge point 0.4238    # permanent charge on the ions

  species_type
    name   Na              # name of the species
    count  160             # number of species 
    mass   22.990          # mass in amu
    mobile True
    charge point 1.000     # permanent charge on the ions

  species_type
    name   Cl              # name of the species
    count  160             # number of species 
    mass   35.453          # mass in amu
    mobile True
    charge point -1.000    # permanent charge on the ions

  species_type 
    name   P               # name of the species
    count  400             # number of species 
    mass   12.0            # mass in amu
    mobile False
    charge neutral         # permanent charge on the ions

  species_type
    name C1                         # name of the species
    count 3649                      # number of species 
    mass 12.0                       # mass in amu
    mobile False                
    charge gaussian 0.955234657 0.0 # gaussian_width (bohr^-1) charge

  species_type
    name C2                         # name of the species
    count 3649                      # number of species 
    mass 12.0                       # mass in amu
    mobile False                
    charge gaussian 0.955234657 0.0 # gaussian_width (bohr^-1) charge


# Molecule definitions
# --------------------
molecules

  molecule_type
    name water       # name of molecule
    count 8800       # number of molecules
    sites O H1 H2    # molecule's sites

    # Rigid constraints
    constraint  O H1 1.88973 # constrained radius for a pair of sites (bohr)
    constraint  O H2 1.88973 # constrained radius for a pair of sites (bohr)
    constraint H1 H2 3.08589 # constrained radius for a pair of sites (bohr)

    constraints_algorithm rattle 1.0e-6 100

# Electrode species definition
# ----------------------------
electrodes

  electrode_type
    name       C1             # name of electrode
    species    C1             # name of the species
    potential -0.018375       # fixed potential (a.u.)

  electrode_type
    name       C2             # name of electrode
    species    C2             # name of the species
    potential +0.018375       # fixed potential (a.u.)

  electrode_charges matrix_inversion  # choice of algorithm for electrode charges
  charge_neutrality true      # enforce charge neutrality

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol 1.0e-5    # coulomb rtol
    coulomb_rcut 41.29     # coulomb cutoff (bohr)
    coulomb_ktol 1.0e-7    # coulomb ktol

  lennard-jones
    lj_rcut  30.4          # lj cutoff (bohr)
    # lj parameters: epsilon in kJ/mol, sigma in angstrom
    lj_pair O  O  0.650200 3.166000
    lj_pair O  Na 0.521578 2.876500
    lj_pair O  Cl 0.521578 3.783500
    lj_pair O  C1 0.391723 3.190000
    lj_pair O  C2 0.391723 3.190000
    lj_pair O  P  0.391723 3.190000
    lj_pair Na Na 0.418400 2.587000
    lj_pair Na Cl 0.418400 3.494000
    lj_pair Na C1 0.314233 2.900500
    lj_pair Na C2 0.314233 2.900500
    lj_pair Na P  0.314233 2.900500
    lj_pair Cl Cl 0.418400 4.401000
    lj_pair Cl C1 0.314233 3.807500
    lj_pair Cl C2 0.314233 3.807500
    lj_pair Cl P  0.314233 3.807500
    lj_pair C1 P  0.236000 3.214000
    lj_pair C2 P  0.236000 3.214000
    lj_pair P  P  0.236000 3.214000

# Output section
# --------------
output
  default 0
  step 10
  lammps 10
  energies 10
  restart 100 alternate
  
