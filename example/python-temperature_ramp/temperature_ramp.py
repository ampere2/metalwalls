import mw

# Increase (or decrease) the temperature using a 'stairs' function

#  Temperature
#       ^
#       |   
# temp2 |            ---
#       |         ---
#       |      --- 
#       |   ---
# temp1 |---
#       ----------------------------->
#       0             num_steps       Time


# Initial temperature 
temp1 = 800

# Final temperature
temp2 = 1500

# Number of time steps at constant temperature
ct = 5
# if ct = 1 the temperature increase is linear 

# Number of steps of the simulation (optional)
# if none the num_steps defined in the runtime is used
num_steps = 1000

# Run the simulation considering the groups defined by 
# first_electrode_list and second_electrode_list as pistons
mw.temp_ramp.run_temp_ramp(temp1, temp2, ct, num_steps)
