#! /usr/bin/env python
###################################################################
#                setup.py                                         #
#  Created : Jan 2017                                             #
#                                                                 #
#  Author:                                                        #
#     Matthieu Haefele                                            #
#     matthieu.haefele@maisondelasimulation.fr                    #
#     Maison de la Simulation USR3441                             #
#                                                                 #
###################################################################


from setuptools import setup




setup(name        = "mw_utils",
      description = "General utility tool that manages/assits in production duties when running Metalwalls",
      author      = "Matthieu Haefele",
      py_modules  = ["mw_utils"],
      scripts=['bin/mw_utils'],
      test_suite  = "tests.test_mw.test_all"
      )
