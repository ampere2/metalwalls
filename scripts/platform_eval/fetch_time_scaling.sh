
CWD=$PWD


for BLOCK in 128 256 512 1024 2048 4096 8192
do
  cd block$BLOCK
  echo "Treating dir block$BLOCK"
  echo "#Node   Time(s)" > res.out
  for NODE in 1 2 
  do
    t=`cat node$NODE/run.out | grep "Total elapsed time:" | tr -s " " | cut -d ' ' -f 4`
    echo $NODE $t >> res.out
  done
  python ~/dev/mw2/scripts/platform_eval/compute_scalability.py
  cat scaling
  cd ..
done


