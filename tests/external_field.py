import unittest
import os.path
import glob
import numpy as np

import mwrun

class test_external_field(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "external_field"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)

  def run_conf(self, confID, nranks):
    path_to_config = os.path.join(self.test_path, confID)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdir = n.workdir
    n.run_mw(nranks)

    ok, msg = n.compare_datafiles("forces.out", "forces.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("charges.out", "charges.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref")
    self.assertTrue(ok, msg)

  def tearDown(self):
    for f in glob.glob(os.path.join(self.workdir, "*.out")):
      os.remove(f)
    if os.path.isfile(os.path.join(self.workdir, "maze_matrix.inpt")):
      os.remove(os.path.join(self.workdir, "maze_matrix.inpt"))
    if os.path.isfile(os.path.join(self.workdir, "hessian_matrix.inpt")):
      os.remove(os.path.join(self.workdir, "hessian_matrix.inpt"))
#    os.remove(os.path.join(self.workdir, "trajectories.xyz"))
#    os.remove(os.path.join(self.workdir, "trajectories.pdb"))
#    os.remove(os.path.join(self.workdir, "trajectories.lammpstrj"))

  def test_conf_electric_field_cg(self):
    self.run_conf("electric_field_cg", 4)

  def test_conf_electric_field_matrix_inversion(self):
    self.run_conf("electric_field_matrix_inversion", 4)

  def test_conf_electric_field_maze_inv(self):
    self.run_conf("electric_field_maze_inv", 4)

  def test_conf_electric_displacement_cg(self):
    self.run_conf("electric_displacement_cg", 4)

  def test_conf_electric_displacement_maze_inv(self):
    self.run_conf("electric_displacement_maze_inv", 4)

  def test_conf_electric_displacement_matrix_inversion(self):
    self.run_conf("electric_displacement_matrix_inversion", 4)
