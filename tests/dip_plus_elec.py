import unittest
import os.path
import glob
import numpy as np

import mwrun

class test_dip_plus_elec(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "dip_plus_elec"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)
    self.setup = False

  def run_conf(self, confID, nranks):
    path_to_config = os.path.join(self.test_path, confID)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdir = n.workdir
    n.run_mw(nranks)
    self.setup = True

    ok, msg = n.compare_datafiles("forces.out", "forces.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("dipoles.out", "dipoles.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("charges.out", "charges.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies.out", "energies.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref")
    self.assertTrue(ok, msg)

  def tearDown(self):
    if self.setup:
      for f in glob.glob(os.path.join(self.workdir, "*.out")):
        os.remove(f)

  def test_conf_LiCl_Al_dip_plus_elec(self):
    self.run_conf("LiCl-Al", 1)

  def test_conf_LiCl_Al_dip_plus_elec_4MPI(self):
    self.run_conf("LiCl-Al", 4)

  def test_conf_LiCl_Al_dip_plus_elec_const_charge(self):
    if mwrun.glob_skip_long_test:
      self.skipTest("Test too long")
    self.run_conf("LiCl-Al_const_charge", 1)

  def test_conf_LiCl_Al_dip_plus_elec_const_charge_4MPI(self):
    if mwrun.glob_skip_long_test:
      self.skipTest("Test too long")
    self.run_conf("LiCl-Al_const_charge", 4)

  def test_conf_LiCl_Al_dip_plus_elec_charge_neutrality(self):
    if mwrun.glob_skip_long_test:
      self.skipTest("Test too long")
    self.run_conf("LiCl-Al_charge_neutrality", 1)

  def test_conf_LiCl_Al_dip_plus_elec_charge_neutrality_4MPI(self):
    if mwrun.glob_skip_long_test:
      self.skipTest("Test too long")
    self.run_conf("LiCl-Al_charge_neutrality", 4)
