

   @test
   subroutine test_setup_pair_atom2atom_local_work_size1()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2atom_local_work => setup_pair_atom2atom_local_work
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 1
      integer :: comm_rank

      ! reference
      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ 2, 2 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)     = reshape((/ 0, 0 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 1, 4, 0, 1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0, 0, 0, 0 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2atom_local_work(localwork, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_atom2atom_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_atom2atom_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2atom_local_work_size1

   @test
   subroutine test_setup_pair_atom2atom_local_work_size2()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2atom_local_work => setup_pair_atom2atom_local_work
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 2
      integer :: comm_rank

      ! reference
      !
      ! 0 x | x x
      ! 0 1 | x x
      ! ---------
      ! 1 1 | 0 x
      ! 0 0 | 1 1

      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ 1, 1, 1, 1 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)     = reshape((/ 0, 0, 1, 1 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 1, 2, 0, 0, 0, 2, 0, 1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0, 2, 0, 0, 0, 0, 0, 0 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2atom_local_work(localwork, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_atom2atom_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_atom2atom_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2atom_local_work_size2

   @test
   subroutine test_setup_pair_atom2atom_local_work_size3()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2atom_local_work => setup_pair_atom2atom_local_work
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 3
      integer :: comm_rank

      ! reference
      !
      ! 0 x | x x
      ! 1 1 | x x
      ! ---------
      ! 2 2 | 2 x
      ! 0 1 | 0 0

      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ 1, 1, 1, 0, 0, 1 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)    = reshape((/ 0, 1, 1, 0, 0, 0 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 0,1,0,1, 1,1,0,0, 0,2,0,0 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0,2,0,0, 0,3,0,0, 0,0,0,0 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2atom_local_work(localwork, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_atom2atom_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_atom2atom_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2atom_local_work_size3

      @test
   subroutine test_setup_pair_atom2atom_local_work_size5()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2atom_local_work => setup_pair_atom2atom_local_work
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 5
      integer :: comm_rank

      ! reference
      !
      ! 0 x | x x
      ! 4 1 | x x
      ! ---------
      ! 0 1 | 2 x
      ! 2 3 | 4 3

      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ &
            1,0, 1,0, 0,1, 0,1, 0,0 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)    = reshape((/ &
            0,0, 1,0, 0,0, 0,1, 0,0 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            0,1,0,0, 0,1,0,0, 0,1,0,0, 0,1,0,0, 1,0,0,1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,0,0,0, 0,1,0,0, 0,2,0,0, 0,3,0,0, 0,0,0,0 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2atom_local_work(localwork, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_atom2atom_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_atom2atom_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2atom_local_work_size5

   @test
   subroutine test_setup_pair_ion2ion_local_work_size1()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2ion_local_work => setup_pair_ion2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 1
      integer :: comm_rank

      ! reference
      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ 2, 2 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)     = reshape((/ 0, 0 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 1, 4, 0, 1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0, 0, 0, 0 /), (/2,2,comm_size/))


      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp, 0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp, 0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp,0.0_wp, .true., .false.)
      ions(1)%offset = 16

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2ion_local_work(localwork, ions, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_ion2ion_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_ion2ion_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2ion_local_work_size1

   @test
   subroutine test_setup_pair_ion2ion_local_work_size2()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2ion_local_work => setup_pair_ion2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 2
      integer :: comm_rank

      ! reference
      !
      ! 0 x | x x
      ! 0 1 | x x
      ! ---------
      ! 1 1 | 0 x
      ! 0 0 | 1 1

      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ 1, 1, 1, 1 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)     = reshape((/ 0, 0, 1, 1 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 1, 2, 0, 0, 0, 2, 0, 1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0, 2, 0, 0, 0, 0, 0, 0 /), (/2,2,comm_size/))


      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2ion_local_work(localwork, ions, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_ion2ion_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_ion2ion_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2ion_local_work_size2

   @test
   subroutine test_setup_pair_ion2ion_local_work_size3()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2ion_local_work => setup_pair_ion2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 3
      integer :: comm_rank

      ! reference
      !
      ! 0 x | x x
      ! 1 1 | x x
      ! ---------
      ! 2 2 | 2 x
      ! 0 1 | 0 0

      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ 1, 1, 1, 0, 0, 1 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)    = reshape((/ 0, 1, 1, 0, 0, 0 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 0,1,0,1, 1,1,0,0, 0,2,0,0 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0,2,0,0, 0,3,0,0, 0,0,0,0 /), (/2,2,comm_size/))


      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp,  0.0_wp, .true., .false.)
      ions(1)%offset = 16

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2ion_local_work(localwork, ions, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_ion2ion_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_ion2ion_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2ion_local_work_size3

      @test
   subroutine test_setup_pair_ion2ion_local_work_size5()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2ion_local_work => setup_pair_ion2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 5
      integer :: comm_rank

      ! reference
      !
      ! 0 x | x x
      ! 4 1 | x x
      ! ---------
      ! 0 1 | 2 x
      ! 2 3 | 4 3

      integer, parameter :: num_block_diag_local(2,0:comm_size-1)  = reshape((/ &
            1,0, 1,0, 0,1, 0,1, 0,0 /), (/2,comm_size/))
      integer, parameter :: diag_iblock_offset(2,0:comm_size-1)    = reshape((/ &
            0,0, 1,0, 0,0, 0,1, 0,0 /), (/2,comm_size/))

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            0,1,0,0, 0,1,0,0, 0,1,0,0, 0,1,0,0, 1,0,0,1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,0,0,0, 0,1,0,0, 0,2,0,0, 0,3,0,0, 0,0,0,0 /), (/2,2,comm_size/))


      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
             0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp,0.0_wp, &
             0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2ion_local_work(localwork, ions, comm_size, comm_rank)

         @assertEqual(num_block_diag_local(:,comm_rank), localwork%pair_ion2ion_num_block_diag_local)
         @assertEqual(diag_iblock_offset(:,comm_rank), localwork%pair_ion2ion_diag_iblock_offset)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2ion_local_work_size5

   @test
   subroutine test_setup_pair_ion2atom_local_work_size1()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2atom_local_work => setup_pair_ion2atom_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 1
      integer :: comm_rank

      ! reference
      !
      ! 0 0 | 0 0
      ! 0 0 | 0 0
      ! ---------
      ! 0 0 | 0 0
      ! 0 0 | 0 0

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 4, 4, 4, 4 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0, 0, 0, 0 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2atom_local_work(localwork, electrodes, ions, comm_size, comm_rank)
         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2atom_local_work_size1

   @test
   subroutine test_setup_pair_ion2atom_local_work_size2()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2atom_local_work => setup_pair_ion2atom_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 2
      integer :: comm_rank

      ! reference
      !
      ! 0 0 | 0 0
      ! 1 1 | 1 1
      ! ---------
      ! 0 0 | 0 0
      ! 0 0 | 1 1
      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            2,2,2,2, 2,2,2,2 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,0,0,0, 2,2,2,2 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2atom_local_work(localwork, electrodes, ions, comm_size, comm_rank)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2atom_local_work_size2

   @test
   subroutine test_setup_pair_ion2atom_local_work_size3()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2atom_local_work => setup_pair_ion2atom_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 3
      integer :: comm_rank

      ! reference
      !
      ! 0 0 | 1 1
      ! 1 2 | 2 0
      ! ---------
      ! 2 2 | 0 0
      ! 0 1 | 1 2

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            2,1,1,2, 1,1,2,1, 1,2,1,1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,2,3,0, 2,3,0,2, 3,0,2,3 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2atom_local_work(localwork, electrodes, ions, comm_size, comm_rank)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2atom_local_work_size3

      @test
   subroutine test_setup_pair_ion2atom_local_work_size5()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_ion2atom_local_work => setup_pair_ion2atom_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 5
      integer :: comm_rank

      ! reference
      !
      ! 0 1 | 4 0
      ! 2 3 | 1 2
      ! ---------
      ! 3 4 | 2 3
      ! 0 1 | 4 0
      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            1,1,1,1, 1,1,1,0, 1,0,1,1, 1,1,0,1, 0,1,1,1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,2,1,3, 1,3,2,0, 2,0,3,0, 3,0,0,1, 0,1,0,2 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_ion2atom_local_work(localwork, electrodes, ions, comm_size, comm_rank)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_ion2atom_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_ion2atom_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_ion2atom_local_work_size5


   @test
   subroutine test_setup_pair_atom2ion_local_work_size1()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2ion_local_work => setup_pair_atom2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 1
      integer :: comm_rank

      ! reference
      !
      ! 0 0 | 0 0
      ! 0 0 | 0 0
      ! ---------
      ! 0 0 | 0 0
      ! 0 0 | 0 0

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ 4, 4, 4, 4 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ 0, 0, 0, 0 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2ion_local_work(localwork, ions, electrodes, comm_size, comm_rank)
         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2ion_local_work_size1

   @test
   subroutine test_setup_pair_atom2ion_local_work_size2()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2ion_local_work => setup_pair_atom2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 2
      integer :: comm_rank

      ! reference
      !
      ! 0 0 | 0 0
      ! 1 1 | 1 1
      ! ---------
      ! 0 0 | 0 0
      ! 0 0 | 1 1
      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            2,2,2,2, 2,2,2,2 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,0,0,0, 2,2,2,2 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2ion_local_work(localwork, ions, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2ion_local_work_size2

   @test
   subroutine test_setup_pair_atom2ion_local_work_size3()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2ion_local_work => setup_pair_atom2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 3
      integer :: comm_rank

      ! reference
      !
      ! 0 0 | 1 1
      ! 1 2 | 2 0
      ! ---------
      ! 2 2 | 0 0
      ! 0 1 | 1 2

      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            2,1,1,2, 1,1,2,1, 1,2,1,1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,2,3,0, 2,3,0,2, 3,0,2,3 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2ion_local_work(localwork, ions, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2ion_local_work_size3

      @test
   subroutine test_setup_pair_atom2ion_local_work_size5()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_pair_atom2ion_local_work => setup_pair_atom2ion_local_work
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      implicit none
      ! Parameters
      ! ----------
      integer, parameter :: num_ion_types = 2
      type(MW_ion_t), allocatable :: ions(:)
      integer, parameter :: num_elec_types = 2
      type(MW_electrode_t), allocatable :: electrodes(:)
      type(MW_localwork_t) :: localwork
      integer, parameter :: comm_size = 5
      integer :: comm_rank

      ! reference
      !
      ! 0 1 | 4 0
      ! 2 3 | 1 2
      ! ---------
      ! 3 4 | 2 3
      ! 0 1 | 4 0
      integer, parameter :: num_block_full_local(2,2,0:comm_size-1)  = reshape((/ &
            1,1,1,1, 1,1,1,0, 1,0,1,1, 1,1,0,1, 0,1,1,1 /), (/2,2,comm_size/))
      integer, parameter :: full_iblock_offset(2,2,0:comm_size-1)    = reshape((/ &
            0,2,1,3, 1,3,2,0, 2,0,3,0, 3,0,0,1, 0,1,0,2 /), (/2,2,comm_size/))

      real(wp) :: voronoi = 0.0_wp

      allocate(ions(num_ion_types))
      call MW_ion_define_type(ions(1), "E1", 16, 0.0_wp,0.0_wp,0.0_wp,0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 0
      call MW_ion_define_type(ions(2), "E2", 16, 0.0_wp,0.0_wp, 0.0_wp, 0.0_wp, &
              0.0_wp, 0.0_wp, .true., .false.)
      ions(1)%offset = 16

      allocate(electrodes(num_elec_types))
      call MW_electrode_define_type(electrodes(1), "E1", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 0
      call MW_electrode_define_type(electrodes(2), "E2", 13, 0.0_wp, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      electrodes(1)%offset = 13

      do comm_rank = 0, comm_size-1
         call MW_localwork_define_type(localwork)
         call MW_localwork_setup_pair_atom2ion_local_work(localwork, ions, electrodes, comm_size, comm_rank)

         @assertEqual(num_block_full_local(:,:,comm_rank), localwork%pair_atom2ion_num_block_full_local)
         @assertEqual(full_iblock_offset(:,:,comm_rank), localwork%pair_atom2ion_full_iblock_offset)

         call MW_localwork_void_type(localwork)
      end do

   end subroutine test_setup_pair_atom2ion_local_work_size5
