# Check run
# =========
# Carbon Werder et al. + SPC/E water
# Na Dang et al. + Cl Dang et al.

# Global simulation parameters
# ----------------------------
num_steps    0         # number of steps in the run
timestep     0.0000001    # timestep (a.u)
temperature  298         # translational temperature
num_pbc      2


# Species definition
# ----------------------
species

  species_type
    name   CAB               # name of the species
    count  320            # number of species
    mass   12.01070000          # mass in amu
    mobile True  
    charge point 0.129   # permanent charge on the ions

  species_type
    name   CAC              # name of the species
    count  320            # number of species
    mass   15.03450000          # mass in amu
    mobile True
    charge point 0.269    # permanent charge on the ions

  species_type
    name   NAA              # name of the species
    count  320            # number of species 
    mass   14.00670000          # mass in amu
    mobile True
    charge point -0.398    # permanent charge on the ions

  species_type
    name CG1                         # name of the species
    count 384                      # number of species 
    mass 12.01                       # mass in amu
    mobile False                
    charge gaussian 104.724169991593 0.01 # wall charge eta

  species_type
    name CG2                         # name of the species
    count 384                      # number of species 
    mass 12.01                       # mass in amu
    mobile False                
    charge gaussian 104.724169991593 -0.01 # wall charge eta


# Electrode species definition
# ----------------------
electrodes

  electrode_type
    name       CG1             # name of electrode
    species    CG1             # name of the species
    potential -0.0       # fixed potential
#    piston 3.443861840114266e-09 +1.0

  electrode_type
    name       CG2             # name of the species
    species    CG2             # name of the species
    potential +0.0       # fixed potential
#    piston 3.443861840114266e-09 -1.0

  electrode_charges constant_charge
  compute_force true

# Interactions definition
# ----------------------
interactions
  coulomb
    coulomb_rtol 1.0e-10
    coulomb_rcut 15.117809003662625
    coulomb_ktol 1.0e-10

  lennard-jones
    lj_rcut  15.117809003662625
    # eps in kJ/mol, sig in angstrom
#    lj_pair  CAB CAB    0.41571596       3.40000000   
#    lj_pair  CAB CAC    0.81250887       3.50000000   
#    lj_pair  CAB CG1    0.3092125        3.38500000   
#    lj_pair  CAB CG2    0.3092125        3.38500000   
#    lj_pair  CAB NAA    0.41571596       3.35000000   
#    lj_pair  CAC CAC    1.58803302       3.60000000   
#    lj_pair  CAC CG1    0.60434993       3.48500000   
#    lj_pair  CAC CG2    0.60434993       3.48500000   
#    lj_pair  CAC NAA    0.81250887       3.45000000   
#    lj_pair  CG1 CG1    0.22999448       3.37000000   
#    lj_pair  CG2 CG2    0.22999448       3.37000000   
#    lj_pair  CG1 CG2    0.22999448       3.37000000   
#    lj_pair  CG1 NAA    0.3092125        3.33500000   
#    lj_pair  CG2 NAA    0.3092125        3.33500000   
#    lj_pair  NAA NAA    0.41571596       3.30000000   

output
  default 0
  charges 1
  forces 1
  step 1
  xyz 1
  elec_forces 1
