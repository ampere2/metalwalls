   ! ================================================================================
   !> Minimum image distance between atoms 1 and a list of atoms 2
   elemental subroutine minimum_image_distance_@PBC@(a, b, c, &
         x_1, y_1, z_1, x_2, y_2, z_2, drnorm2)
      implicit none
      ! Parameters
      ! ----------
      real(wp), intent(in) :: a, b, c    !< structure
      real(wp), intent(in) :: x_1, y_1, z_1 !< xyz atom 1 position
      real(wp), intent(in) :: x_2, y_2, z_2 !< xyz atom 2 positions

      real(wp), intent(inout) :: drnorm2         !< square of minimum image distance

      real(wp) :: dx, dy, dz

      call minimum_image_displacement_@PBC@(a, b, c, &
            x_1, y_1, z_1, x_2, y_2, z_2, dx, dy, dz, drnorm2)

   end subroutine minimum_image_distance_@PBC@
