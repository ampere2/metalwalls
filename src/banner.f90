module MW_banner

   implicit none
   private

   public :: MW_banner_print

contains

   !===============================================================================
   ! print banner to ounit
   subroutine MW_banner_print(ounit)
      implicit none
      integer, intent(in) :: ounit                ! output unit
      write(ounit, '(a)') "**************************************************************************"
      write(ounit, '(a)') "**************************************************************************"
      write(ounit, '(a)') "**                                                                      **"
      write(ounit, '(a)') "**                                                                      **"
      write(ounit, '(a)') "**                              METAL WALLS                             **"
      write(ounit, '(a)') "**                                                                      **"
      write(ounit, '(a)') "**                                                                      **"
      write(ounit, '(a)') "**************************************************************************"
      write(ounit, '(a)') "**************************************************************************"
   end subroutine MW_banner_print

end module MW_banner
