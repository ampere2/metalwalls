program metalwalls
   ! Metal Walls main program
   use MW_parallel, only: MW_parallel_t
   use MW_system, only: MW_system_t
   use MW_algorithms, only: MW_algorithms_t
   use MW_tools, only : MW_tools_initialize => initialize, &
         MW_tools_start_parallel => start_parallel, &
         MW_tools_setup => setup, &
         MW_tools_run_step => run_step, &
         MW_tools_finalize => finalize, &
         MW_tools_finalize_parallel => finalize_parallel

   implicit none

   type(MW_parallel_t) :: parallel
   type(MW_system_t) :: system
   type(MW_algorithms_t) :: algorithms
   logical :: do_output
   integer :: step_output_frequency, istep=0

   call MW_tools_start_parallel(parallel)
   call MW_tools_initialize(system, parallel, algorithms, do_output)
   call MW_tools_setup(system, parallel, algorithms, do_output, step_output_frequency)
   call MW_tools_run_step(system, parallel, algorithms, istep, system%num_steps, &
         do_output, step_output_frequency, .True., .True.)
   call MW_tools_finalize(system, parallel, algorithms)
   call MW_tools_finalize_parallel()


end program metalwalls
